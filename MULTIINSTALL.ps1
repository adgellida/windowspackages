#This script is part of
#https://github.com/adgellida/windowspackages
#GNU General Public License v2.0

cinst -y chocolateygui
cinst -y megasync
cinst -y open-shell
cinst -y teamviewer
cinst -y chocolateygui
cinst -y winrar
#cinst -y volume2.install
cinst -y winsys
cinst -y keepassxc
cinst -y vlc
cinst -y sublimetext4
cinst -y smartgit
cinst -y google-chrome
cinst -y qbittorrent
cinst -y skype
cinst -y steam-client
cinst -y telegram.install
cinst -y freefilesync
cinst -y audacity
cinst -y drawio
cinst -y crystaldiskmark
cinst -y crystaldiskinfo.install
cinst -y malwarebytes
cinst -y vscode.install
cinst -y stopupdates10
cinst -y origin
cinst -y everything
cinst -y aida64-extreme
cinst -y spotify
cinst -y slack
cinst -y parsec
cinst -y filezilla
cinst -y element-desktop
cinst -y putty.install
cinst -y blender
cinst -y firefox
cinst -y vivaldi
cinst -y teamspeak
cinst -y vscode
cinst -y openhardwaremonitor
cinst -y anydesk.install
cinst -y discord.install
cinst -y sumo
cinst -y deepl
cinst -y xampp-80
cinst -y msiafterburner
cinst -y unity-hub
cinst -y partitionwizard
cinst -y zoom
cinst -y windirstat
cinst -y voicemeeter-potato.install
cinst -y winpcap
cinst -y vysor
cinst -y streamlabs-obs
cinst -y supertuxkart
cinst -y virtualbox virtualbox-guest-additions-guest.install
cinst -y bluescreenview.install
cinst -y mtputty
cinst -y kicad
cinst -y jdownloader
cinst -y freecad
cinst -y frostwire
cinst -y jre8
cinst -y dropbox
cinst -y epicgameslauncher
cinst -y converseen
cinst -y brave
cinst -y authy.install
cinst -y androidstudio
cinst -y etcher
cinst -y macrocreator.install
cinst -y processhacker.install
cinst -y krita

##not exist
#kasperskysecurityclod
#prismlivestudio
#yawcam
#revolt
#converseen

##others
#spybot
#superantispyware
#obs-studio.install
#stopupdates10
#acestream
#sumatrapdf.install
#libreoffice
#adblockplusie #not necessary on edge
#googlechrome.dev
#googlechrome
#ublock #to add
#adblockpluschrome #changed by ublock
#hyper
#notepadplusplus.install
#nettime
#clover
#ccleaner
#silverlight
#peazip
#qtcreator
#choco-upgrade-all-at-startup
#dirsyncpro